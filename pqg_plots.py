#!/usr/bin/env python3

from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph.opengl as gl
import pyqtgraph as pg
import numpy as np

def show2Dplot(data,columns,title='',header=[],showMarkers=False,scatter=False,save_only=None):
	X = data[:,columns[0]]
	Y = data[:,columns[1]]
	pg.setConfigOptions(antialias=True)
	if showMarkers or scatter:
		marker='x'
	else:
		marker=None
	if scatter:
		plot = pg.plot(x=X, y=Y,symbol=marker,pen=None)
	else:
		plot = pg.plot(x=X, y=Y,symbol=marker)
	plot.showGrid(x=True, y=True)
	plot.setLabel('bottom',header[columns[0]])
	plot.setLabel('left',header[columns[1]])
	plot.setTitle("Justplotit - "+title)
	if save_only==None:
		QtGui.QApplication.instance().exec_()
	else:
		import pyqtgraph.exporters
		# use this workaround due to a bug in the pyqtgraph library
		# replace it with this once it's solved exporter = pg.exporters.ImageExporter(plot.plotItem)
		from pqg_imageExporter import PQG_ImageExporter
		exporter = PQG_ImageExporter(plot.plotItem)
		exporter.export(save_only)

def show3Dplot(data,columns,title='',header=[],showMarkers=False,save_only=None,plot3d=False):
	X = data[:,columns[0]]
	Y = data[:,columns[1]]
	Z = data[:,columns[2]]
	# pts=np.vstack([X,Y,Z]).transpose()
	z_min, z_max = Z.min(), Z.max()
	cols=np.array([(Z-z_min)/(z_max-z_min),1.0-(Z-z_min)/(z_max-z_min),np.zeros(len(Z))]).transpose()
	pg.setConfigOptions(antialias=True)
	if showMarkers:
		marker='x'
	else:
		marker=None
	## Create a GL View widget to display data
	app = QtGui.QApplication([])
	if plot3d:
		w = gl.GLViewWidget()
		w.setWindowTitle("Justplotit - "+title)
		pts=np.vstack([X,Y,Z]).transpose()
		plot = gl.GLScatterPlotItem(pos=pts,color=cols)
		w.addItem(plot)
		w.show()
		QtGui.QApplication.instance().exec_()
	else:
		mw = QtGui.QMainWindow()
		mw.resize(800,800)
		view = pg.GraphicsLayoutWidget()  ## GraphicsView with GraphicsLayout inserted by default
		mw.setCentralWidget(view)
		mw.setWindowTitle("Justplotit - "+title)
		w1 = view.addPlot()
		s1 = pg.ScatterPlotItem(pen=pg.mkPen(None))
		spots=[{'pos':(X[i],Y[i]),'brush':pg.mkBrush(255*cols[i])} for i in range(len(X))]
		s1.addPoints(spots)
		w1.addItem(s1)
		w1.setLabel('bottom',header[columns[0]])
		w1.setLabel('left',header[columns[1]])
		w1.setTitle(header[columns[2]]+": green="+str(z_min)+" >>>> red="+str(z_max))
		mw.show()	
		if save_only==None:
			QtGui.QApplication.instance().exec_()
		else:
			import pyqtgraph.exporters
			# use this workaround due to a bug in the pyqtgraph library
			# replace it with this once it's solved exporter = pg.exporters.ImageExporter(plot.plotItem)
			from pqg_imageExporter import PQG_ImageExporter
			exporter = PQG_ImageExporter(w1)
			exporter.export(save_only)

class CandlestickItems(pg.GraphicsObject):
	def __init__(self, data, color='w'):
		pg.GraphicsObject.__init__(self)
		self.data = data  ## data must have fields: time, open, close, min, max
		self.color=color
		self.generatePicture()
	
	def generatePicture(self):
		## pre-computing a QPicture object allows paint() to run much more quickly, 
		## rather than re-drawing the shapes every time.
		self.picture = QtGui.QPicture()
		p = QtGui.QPainter(self.picture)
		i=0
		w=len(self.data)*.02
		for (name, vmin, q1, med, q3, vmax) in self.data:
			p.setPen(pg.mkPen(self.color))
			p.drawLine(QtCore.QPointF(i, vmin), QtCore.QPointF(i, q1))
			p.drawLine(QtCore.QPointF(i, q3), QtCore.QPointF(i, vmax))
			p.drawLine(QtCore.QPointF(i-w*.9, vmin), QtCore.QPointF(i+w*.9, vmin))
			p.drawLine(QtCore.QPointF(i-w*.9, vmax), QtCore.QPointF(i+w*.9, vmax))
			p.drawRect(QtCore.QRectF(i-w, q1, w*2, q3-q1))
			p.setPen(pg.mkPen('r'))
			p.drawLine(QtCore.QPointF(i-w, med), QtCore.QPointF(i+w, med))
			i+=1
		p.end()

	def paint(self, p, *args):
		p.drawPicture(0, 0, self.picture)
  
	def boundingRect(self):
		## boundingRect _must_ indicate the entire area that will be drawn on
		## or else we will get artifacts and possibly crashing.
		## (in this case, QPicture does all the work of computing the bouning rect for us)
		return QtCore.QRectF(self.picture.boundingRect())

def show5valuesSummaryPlot(data,names,title='',save_only=None):
	pg.setConfigOptions(antialias=True)
	plot = pg.plot()
	if save_only==None:
		item = CandlestickItems(data,color='w')
		plot.setTitle("Justplotit - "+title)
	else:
		item = CandlestickItems(data,color='b')
	plot.showGrid(x=False, y=True)
	xdict = dict(enumerate(names))
	plot.getAxis("bottom").setTicks([xdict.items()])
	plot.addItem(item)
	# pg.GraphicsWindow().addPlot(axisItems={'bottom': stringaxis})
	if save_only==None:
		QtGui.QApplication.instance().exec_()
	else:
		import pyqtgraph.exporters
		# use this workaround due to a bug in the pyqtgraph library
		# replace it with this once it's solved exporter = pg.exporters.ImageExporter(plot.plotItem)
		from pqg_imageExporter import PQG_ImageExporter
		exporter = PQG_ImageExporter(plot.plotItem)
		exporter.export(save_only)