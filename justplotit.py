#!/usr/bin/env python3

###############
# Code developed by Luca Baronti
# Please refer to the repository for furthoer information 
# https://gitlab.com/luca.baronti/justplotit
###############

import sys, argparse, importlib, time
import math as mt
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import griddata

pyqtgraph_present = importlib.util.find_spec('pyqtgraph') != None
if pyqtgraph_present:
	import pqg_plots as pqg

# try:
# 	pyqtgraph_present = importlib.util.find_spec('pyqtgraph') is None
# 	import pqg_plots as pqg
# 	pyqtgraph_present = True
# except ImportError:
# 	pyqtgraph_present = False

# try:
# 	importlib.find_module('watchdog')
# 	from watchdog.observers import Observer
# 	from watchdog.events import FileSystemEventHandler
# 	watchdog_present = True
# except ImportError:
# 	watchdog_present = False

def generateExamplesCSV():
	f=open("examples/2dsinx.csv",'w')
	f.write("x,sin(x)\n")
	for x in np.arange(-mt.pi,mt.pi,mt.pi/50):
		f.write(','.join([str(v) for v in [x,mt.sin(x)]])+'\n')
	f.close()
	f=open("examples/3dsinxy.csv",'w')
	f.write("x,y,sin(x*y)\n")
	for x in np.arange(-mt.pi,mt.pi,mt.pi/20):
		for y in np.arange(-mt.pi,mt.pi,mt.pi/20):
			f.write(','.join([str(v) for v in [x,y,mt.sin(x*y)]])+'\n')
	f.close()
	f=open("examples/3dsinxycosxy.csv",'w')
	f.write("x,y,sin(x*y),cos(x*y)\n")
	for x in np.arange(-mt.pi,mt.pi,mt.pi/20):
		for y in np.arange(-mt.pi,mt.pi,mt.pi/20):
			f.write(','.join([str(v) for v in [x,y,mt.sin(x*y),mt.cos(x*y)]])+'\n')
	f.close()
	f=open("examples/5vsummary.csv",'w')
	f.write("name,min,q1,median,q3,max\n")
	for i in range(7):
		vals= [np.random.random_sample()*10 for _ in range(5)]
		vals.sort()
		f.write("sample"+str(i)+','+','.join([str(x) for x in vals])+"\n")
	f.close()

def makeTitle(path):
	if path.rfind('/')!=-1:
		path=path[path.rfind('/')+1:]
	if path.rfind('.')!=-1:
		path=path[:path.rfind('.')]
	return path

def show2Dplot(data,columns,title='',header=[],showMarkers=False,scatter=False,scatter3d=False,save_only=None):
	X = data[:,columns[0]]
	Y = data[:,columns[1]]
	if scatter and scatter3d:
		Z = data[:,columns[2]]
		z_min=min(Z)
		z_max=max(Z)
		z_norm=lambda x:x-z_min/(z_max-z_min)
		z_colors=[]
		for el in Z:
			v=z_norm(el)
			z_colors+=[(v,1.0-v,0.0)]
	plt.figure(num="Justplotit - "+title)
	plt.suptitle(title)
	if showMarkers or scatter:
		marker='x'
	else:
		marker=''	
	if scatter:
		if scatter3d:
			plt.scatter(X,Y,marker=marker,c=Z,cmap=plt.cm.rainbow)
			cbar = plt.colorbar() # Z-legend
			cbar.set_label(header[columns[2]]) 
		else:
			plt.scatter(X,Y,marker=marker)
	else:
		plt.plot(X,Y,marker=marker)
	plt.grid(True, which='both')
	if len(header)==0:
		header=['X','Y']
	plt.xlabel(header[columns[0]])
	plt.ylabel(header[columns[1]])

	if save_only==None:
		plt.show()
	else:
		plt.savefig(save_only)
		
def showHeatMap(data,columns,title='',header=[],showMarkers=False,save_only=None):
	X = data[:,columns[0]]
	Y = data[:,columns[1]]
	Z = data[:,columns[2]]
	# find avg of Z as baseline
	z_avg=0.0
	for el in Z:
		z_avg+=el
	z_avg/=len(Z)
	# define the grid
	xmin=np.amin(X)
	xmax=np.amax(X)
	dx=0.01 # x-interval
	ymin=np.amin(Y)
	ymax=np.amax(Y)
	dy=0.01 # y-interval
	nx=int(xmax-xmin)/dx + 1
	ny=int(ymax-ymin)/dx + 1
	# generate data points
	xi = np.linspace(xmin,xmax,nx)
	yi = np.linspace(ymin,ymax,ny)
	xi, yi = np.mgrid[xmin:xmax:100j, ymin:ymax:200j]
	zi = griddata((X,Y),Z,(xi,yi),method='linear')
	zi=np.where(zi!=zi, z_avg, zi) # replace NaN values witht he average
	# Plot definitions
	plt.figure(num="Justplotit - "+title)
	plt.suptitle(title)
	plt.contour(xi,yi,zi,15,linewidths=0.5,colors='k') # hight lines
	plt.contourf(xi,yi,zi,15,cmap=plt.cm.rainbow, vmax=zi.max(), vmin=zi.min()) # heat map
	
	cbar = plt.colorbar() # Z-legend
	if showMarkers:
		marker='x'
	else:
		marker=''
	plt.scatter(X,Y,marker=marker,c='b',s=5,zorder=10) # plot the actual data points
	# set caption for legend
	if len(header)==0:
		header=['X','Y','Z']
	plt.xlabel(header[columns[0]])
	plt.ylabel(header[columns[1]])		
	cbar.set_label(header[columns[2]]) 
	# set ranges
	plt.xlim(xmin,xmax)
	plt.ylim(ymin,ymax)

	if save_only==None:
		plt.show()
	else:
		plt.savefig(save_only)

def determineDelimiter(path):
	f=open(path)
	line=f.readline().strip()
	f.close()
	d=','
	if len(line.split(d))==1:
		d='\t'
	else:
		return d
	if len(line.split(d))==1:
		d=' '
	else:
		return d
	if len(line.split(d))==1:
		raise ValueError("Can't determine the delimiter in file "+path)
	else:
		return d

def readHeader(path,delimiter=','):
	f=open(path)
	header=f.readline().strip().split(delimiter)
	try:
		float(header[0])
		header=[]
	except ValueError:
		pass
	f.close()
	return header

def run_program(args):
	if not pyqtgraph_present:
		args.pyqtgraph=False
	elif args.fiveValuesSummary and args.scatter3d: # if scatter3d or fiveValuesSummary options are selected, the plot must be pyqtgraph
		args.pyqtgraph=True
	csv_file=args.csv_file
	delimiter=determineDelimiter(csv_file)
	header=readHeader(csv_file,delimiter)
	title=makeTitle(csv_file)
	# read data
	if len(header)==0:
		data = np.genfromtxt(csv_file, delimiter=delimiter)
	else:
		data = np.genfromtxt(csv_file, skip_header=1, delimiter=delimiter)
	if len(data)==0:
		sys.stderr.write("CSV file "+csv_file+" empty\n")
		sys.exit(1)
	if len(data[0])<2:
		sys.stderr.write("CSV file "+csv_file+" contains only one column\n")
		sys.exit(1)

	if pyqtgraph_present and args.fiveValuesSummary:
		# load the first column entries as strings
		f=open(csv_file)
		names=[]
		first_line=True
		for line in f.readlines():
			if first_line and len(header)>0:
				first_line=False
				continue
			names+=[line.strip().split(delimiter)[0]]
		pqg.show5valuesSummaryPlot(data,names,title=title,save_only=args.s)
		sys.exit(0)
	# check the column indeces
	if args.c==None:
		if len(data[0])==2:
			columns=[0,1]
		else:
			columns=[0,1,2]
	else:
		columns=[int(x) for x in args.c.split(',')]
		if len(columns)<2 or len(columns)>3:
			sys.stderr.write("The number of columns must be either 2 or 3, found "+str(len(columns))+'\n')
			sys.exit(1)
		for c in columns:
			if c>=len(data[0]):
				sys.stderr.write("Asked to plot column of index "+str(c)+" with only "+str(len(data[0]))+" columns (the columns are 0-indexed).\n")
				sys.exit(1)

	if len(columns)==2:
		if pyqtgraph_present and args.scatter3d:
			sys.stderr.write("Can't use the -scatter3d option with only 2 columns selected.\n")
			sys.exit(1)
		if pyqtgraph_present and args.pyqtgraph:
			pqg.show2Dplot(data,columns,title=title,header=header,showMarkers=args.m,scatter=args.scatter,save_only=args.s)
		else:
			show2Dplot(data,columns,title=title,header=header,showMarkers=args.m,scatter=args.scatter,scatter3d=False,save_only=args.s)
	else: 
		if args.heat:
			showHeatMap(data,columns,title=title,header=header,showMarkers=args.m,save_only=args.s)
		elif args.pyqtgraph:
			pqg.show3Dplot(data,columns,title=title,header=header,showMarkers=args.m,save_only=args.s,plot3d=args.scatter3d)
		else:
			show2Dplot(data,columns,title=title,header=header,showMarkers=args.m,scatter=True,scatter3d=True,save_only=args.s)

# class MyHandler(FileSystemEventHandler):

# 	def __init__(self, path):
# 		self.path=path

# 	def on_modified(self, event):
# 		if event.src_path==self.path:
# 			print(dir(plt))
# 			plt.close('all')


if __name__=='__main__':
	description = 'Simple plotter of 2D and 3D data from a CSV (Comma Separated Values) file. \
								It plots the first 3 columns in the given file in a coloured scatter plot or, if only 2 columns are present, a line graph is plotted instead. \
								In order to plot specific columns, use the -c option described belows.'
	if not pyqtgraph_present:
		description+="\nWARNING: the module pyqtgraph is not present, please install it in order to access to advanced functionalities."
	parser = argparse.ArgumentParser(description=description)
	parser.add_argument('csv_file', metavar='csv_file', type=str, 
                    help='Comma Separated Values file containing the data to be plotted.')
	parser.add_argument('-m',  action='store_true',
                    help='Add point markers to the plot. Used only for line plots and heatmaps.')
	parser.add_argument('-c', metavar='columns', type=str,
                    help='Specify the columns used to plot, useful to plot specific columns on files with more than 3 columns. \
                    			The format is x,y,z where x y and z are 0-indexed column numbers separated by a comma, with z optional.')
	parser.add_argument('-s', metavar='save_path',  type=str,
                    help='Save the plot as image to the target path and exit. No graph will be plotted in this case, only saved.')
	parser.add_argument('-heat', action='store_true',
                    help='Display the the heatmap of the given data. In this case, 3 columns must be used. \
                     A grid is need to be build and missing values will be replaced with the average.')
	parser.add_argument('-scatter', action='store_true',
                    help="Display the the data as scatter plot. This is the default behaviour if 3 columns are selected, \
                    and it's used to display the data as scatter plot when 2 columns are used instead (they are plotted as a line per default).")
	if pyqtgraph_present:
		parser.add_argument('-pqg', '--pyqtgraph', action='store_true',
	                    help='Display the graphs using PyQtGraph. This option is suggested to handle a large ammount of data.')
		parser.add_argument('-scatter3d', action='store_true',
	                    help='Display the data as 3d scatter plot. In this case, 3 columns must be used and the -s parameter, if present, will be ignored.')
		parser.add_argument('-v5', "--fiveValuesSummary", action='store_true',
	                    help='Display the five values summary included in the passed csv as 2d candlestick plot.')
		
	args = parser.parse_args()
	run_program(args)
	# event_handler = MyHandler("/tmp/prova")
	# observer = Observer()
	# observer.schedule(event_handler, path='/tmp/', recursive=False)
	# observer.start()
	# try:
	#     while True:
	#         time.sleep(1)
	# except KeyboardInterrupt:
	#     observer.stop()
	# observer.join()
