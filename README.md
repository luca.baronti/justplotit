# Justplotit

Simple plotter of 2D and 3D data from a data file wrote in Python 3.
This tool can read CSV (Comma Separated Values) file format, as well as other formats that use tabs or spaces as separator, with or without an header line, and plot the data in a convenient graphical form.

If the file presents only 2 columns, a line plot is displayed.
If the file presents 3 columns, a coloured scatter plot is displayed instead.

Arguments can be used to override this default behaviour.
For instance, the option *-scatter* can be passed to force the display of a scatter plot for data with only 2 columns.
For CSV files with many columns, it's possible to specify which columns must be used with the *-c* argument.

This tool uses two different engines under the hood, *matplotlib* and the optional *pyqtgraph*, with the option to switch from one to the other with an argument.

## Dependencies
As per default, this tool use the [MatPlotlib](https://matplotlib.org/) library for visualisation purposes.
This library can be installed using **pip3** in most systems the following way:
```sh
$ pip3 install matplotlib
```
### Optional Libraries
If the more advanced [PyQtGraph](http://www.pyqtgraph.org/) library is installed, it can be used as plotting engine with the *--pyqtgraph* command parameter.
In this case further options are available, like the *-scatter3d* parameter, which will visualise the data as a 3d scatter plot.

To install the PyQtGraph library it's possible to use **pip3**:
```sh
$ pip3 install pyqtgraph
```
In this case, the python3 pyqt4 OpenGL library is also necessary. 
On Ubuntu systems it can be installed the following way:
```sh
$ sudo apt install python3-pyqt4.qtopengl
```
# Usage

To following command:
```sh
$ ./justplotit.py myfile.csv
```
will plot the data contained in *myfile.csv*.
As default, the data in *myfile.csv* need to have the values separated with a comma as delimiter and each pattern must be separated by a newline.
If the values are not separated by a comma, the tool will automatically try to parse the values with *tab* and *space* as potential delimiters.
If an header is present in the first line, it will be showed in the plot, otherwise default labels will be used instead.

The plot showed will be either a 2d Carthesian plot or a 3d heat map, according the number of columns present in the file (2 and 3, respectively).
The plot boundaries are set according the maximum and minimum values of each column, included the heat map color.

## Optional Arguments
I'ts possible to add the following otional arguments at command line:

- **-m** show point markers on the plot;
- **-c x,y,z** specify the columns used to plot, useful to plot specific columns on files with more than 3 columns. The format is x,y,z where x y and z are 0-indexed column numbers separated by a comma, with z optional;
- **-s save_path** Save the plot as image to the target path and exit. No graph will be plotted in this case, only saved. It's an useful option to use in a script;
- **-heat** Display the the heatmap of the given data. In this case, 3 columns must be used. A grid is need to be build and missing values will be replaced with the average;
- **-pqg, --pyqtgraph** Display the graphs using PyQtGraph. This option is suggested to handle many data;
- **-scatter** Display the the data as scatter plot. This is the default behaviour if 3 columns are selected, and it's used to display the data as scatter plot when 2 columns are used instead (they are plotted as a line per default);
- **-scatter3d** Display the data as 3d scatter plot. In this case, 3 columns must be used and the -s parameter, if present, will be ignored;
- **-v5** Display the five values summary included in the passed CSV as 2d candlestick plot;

To get a summary of the most updated features available, just run
```sh
$ ./justplotit.py -h
```
# Examples
Some example CSV files are included in this repository under the *examples* directory.

The following command:
```sh
$ ./justplotit.py examples/2dsinx.csv
```
will generate the following plot:

<img src="pics/2dsinx.png"
     align="centre"
     width="600" height="400" />
     
whilst the following command:
```sh
$ ./justplotit.py -heat examples/3dsinxy.csv
```
will generate the following plot:

<img src="pics/3dsinxy.png"
     align="centre"
     width="600" height="500" />

If the PyQtGraph library is present, the following command can be used
```sh
$ ./justplotit.py -scatter3d examples/3dsinxy.csv
```
to generate a 3D rapresentation of the data:

<img src="pics/3dsinxyScatter.png"
     align="centre"
     width="600" height="500" />

If a file contains a list of [Five Values Summaries](https://en.wikipedia.org/wiki/Five-number_summary), where each row starts with a sample name, and the following fields are the minimum, first quartile, median, third quatile and maximum values of the distribution, it can be plotted in a form of candlesticks using the *-v5* flag.

As an example, the following command:
```sh
$ ./justplotit.py -v5 examples/5vsummary.csv
```
generate the following graph:

<img src="pics/5vsummary.png"
     align="centre"
     width="700" height="500" />

